module.exports = {
  "env": {
    "es6": true,
    "node": true,
    "browser": true
  },
  "extends": "airbnb",
  "parser": "babel-eslint",
  "globals": {
      "Atomics": "readonly",
      "SharedArrayBuffer": "readonly"
  },
  "parserOptions": {
      "ecmaFeatures": {
        "jsx": true
      },
      "ecmaVersion": 2018,
      "sourceType": "module"
  },
  "plugins": [
    "react",
  ],
  "rules": {
      "react/jsx-filename-extension": "off",
      "react/prefer-stateless-function": [0, { "ignorePureComponents": false }],
      "react/prop-types": [
        "error",
        {
          "ignore":
          [
            "navigation",
            "dispatch",
            "booking",
            "auth",
            "teeTime",
            "players",
            "addOn",
            "golfCart",
            "addOns",
            "playerTypes",
            "payment",
            "bookingDetails",
            "inGame",
            "golfCarts",
            "vouchers",
            "serviceCharge",
            "children",
            "history",
            "match",
            "params"
          ]
        }
      ],
      "quotes": [0, "double", { "avoidEscape": true }],
      "no-underscore-dangle":  ["error", { "allow": ["_navigator", "_id", "_root"] }],
      "arrow-parens": ["error", "as-needed"],
      "import/no-unresolved": 2,
      "no-console": "off",
      "max-len": 0,
      "react/forbid-prop-types": [0],
      "no-unused-expressions":  ["error", { "allowShortCircuit": false, "allowTernary": true }],
      'semi': [0, 'always'],
      "vars-on-top": 2,
      "require-await": 2,
      "no-await-in-loop": 2,
      "no-return-await": 2,
      "camelcase": [
        "error",
        {
          "allow":
          [
            "UNSAFE_componentWillMount",
            "UNSAFE_componentWillUpdate"
          ]
        }
      ],
      "no-multi-spaces": ["error", { "ignoreEOLComments": false }],
      "func-style": ["error", "declaration", { "allowArrowFunctions": true }],
      "linebreak-style": [0, "unix"],
      "react/jsx-props-no-spreading": [0, {
        "custom": "ignore",
      }],
  },
  "settings": {
    "import/resolver": {
      "babel-module": {}
    }
  }
};