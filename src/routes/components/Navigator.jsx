import React, { Component } from 'react'

import {
  Link,
} from 'react-router-dom'

import { connect } from 'react-redux'

class Navigator extends Component {
  render() {
    const { auth } = this.props;
    return (
      <div style={{
        width: '100%',
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: '#000',
        zIndex: 1,
        position: 'absolute',
        height: 50,
      }}
      >
        <div
          style={{
            width: '40%',
            marginLeft: 40,
          }}
        >
          <Link
            to="/"
            style={{ textDecoration: 'none' }}
          >
            <div>
              <p
                style={{
                  color: 'white',
                  fontFamily: 'routeFont',
                  fontSize: '40px',
                }}
              >
                {process.env.REACT_APP_NAME}
              </p>
            </div>
          </Link>
        </div>
        <div
          style={{
            width: '40%',
            marginRight: 40,
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            backgroundColor: 'transparent',
          }}
        >
          <div />
          {auth.isAuth && (
            <Link
              to="/publish"
              style={{ textDecoration: 'none' }}
            >
              <div>
                <p
                  style={{
                    color: 'white',
                    fontFamily: 'routeFont',
                    fontSize: '20px',
                  }}
                >
                  Publish a note
                </p>
              </div>
            </Link>
          )}
        </div>
      </div>
    )
  }
}

const mapStateToProps = data => ({ auth: data.authenticate })

export default connect(mapStateToProps)(Navigator);
