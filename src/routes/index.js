import React from "react";

import {
  Router,
  Switch,
  Route,
} from "react-router-dom";

import {
  Home,
  Publishing,
  Edit,
  Login,
} from '../screens'

import styles from './route.module.css'
import Navigator from "./components/Navigator";
import history from './history'

class App extends React.Component {
  render() {
    return (
      <Router history={history}>
        <div className={styles.example}>
          <Navigator />

          <div style={{ marginTop: 50 }}>
            {/* A <Switch> looks through its children <Route>s and
                renders the first one that matches the current URL. */}
            <Switch>
              <Route path="/login">
                <Login />
              </Route>
            </Switch>
            <Switch>
              <Route path="/blogs/edit/:slug">
                <Edit />
              </Route>
              <Route path="/publish">
                <Publishing />
              </Route>
              <Route path="/">
                <Home />
              </Route>
            </Switch>
          </div>
        </div>
      </Router>
    );
  }
}

export default App;
