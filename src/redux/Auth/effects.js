/* eslint-disable consistent-return */
import { request } from "../../helpers";

export const register = async params => {
  try {
    const {
      firstName,
      lastName,
      country,
      birthday,
      gender,
      email,
      password,
    } = params;

    const regData = new FormData();
    regData.append('firstName', firstName);
    regData.append('lastName', lastName);
    regData.append('country', country);
    regData.append('birthday', birthday);
    regData.append('gender', gender);
    regData.append('email', email);
    regData.append('password', password);

    const data = await request.post('/register', regData);
    // console.log(data)
    return data.data;
  } catch (e) {
    console.log(`authUser responded with ${e}`);
  }
}

export const authUser = async params => {
  try {
    const formData = new FormData();
    formData.append('email', params.email.trim());
    formData.append('password', params.password);
    const data = await request.post('/auth/login', formData);
    // console.log('auth', data)
    return data.data;
  } catch (e) {
    console.log(`authUser responded with ${e}`);
  }
}

export const fetchUserData = async params => {
  try {
    const data = await request.get('/auth/user', {
      headers: {
        Authorization: `Bearer ${params.token}`,
      },
    });
    return data.data;
  } catch (e) {
    console.log(`fetchUserData responded with ${e}`);
  }
}

export const logoutUser = async params => {
  try {
    const data = await request.delete('/auth/logout', {
      headers: {
        Authorization: `Bearer ${params.token}`,
      },
    });
    // console.log('logout data', data)
    return data.data;
  } catch (e) {
    console.debug('logoutUser responded with ', e)
  }
}
