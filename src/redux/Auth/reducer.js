import * as types from "./action-types";

const initialState = {
  isAuth: false,
  requestingAuth: false,
  requestingSignup: false,
  clearingAuth: false,
  authSession: null,
  authError: "",
  signupError: [],
  requestingRestore: true,
  logoutError: "",
  fetchingUser: false,
  fetchUserFailed: false,
};

export default function authenticate(state = initialState, action = {}) {
  switch (action.type) {
    case types.LOGIN_REQUEST:
      return {
        ...state,
        requestingAuth: true,
        authError: "",
      };
    case types.LOGIN_SUCCESS:
      return {
        ...state,
        isAuth: true,
        authSession: action.data.session,
        requestingRestore: false,
        requestingAuth: false,
      };
    case types.LOGIN_FAILED:
      return {
        ...state,
        requestingAuth: false,
        authError: action.data.error,
      };
    case types.SIGNUP_REQUEST:
      return {
        ...state,
        isAuth: false,
        requestingSignup: true,
        signupError: "",
      };
    case types.SIGNUP_SUCCESS:
      return {
        ...state,
        requestingSignup: false,
      };
    case types.SIGNUP_FAILED:
      return {
        ...state,
        requestingSignup: false,
        signupError: action.data.error,
      };
    case types.RESET_REGISTRATION_ERRORS:
      return {
        ...state,
        signupError: [],
      }
    case types.LOGOUT_REQUEST:
      return {
        ...state,
        clearingAuth: true,
        authError: "",
      };
    case types.LOGOUT_SUCCESS:
      return {
        ...state,
        isAuth: false,
        clearingAuth: false,
        authSession: null,
        authError: "",
      };
    case types.LOGOUT_FAILED:
      return {
        ...state,
        clearingAuth: false,
        authError: action.data.error,
      };
    case types.RESTORE_REQUEST:
      return {
        ...state,
        requestingRestore: true,
      };
    case types.RESTORE_FAILED:
      return {
        ...state,
        requestingRestore: false,
      };
    default:
      return state;
  }
}
