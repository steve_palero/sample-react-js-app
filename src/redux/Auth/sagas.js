import {
  call,
  put,
  takeLatest,
  delay,
} from 'redux-saga/effects';

import history from '../../routes/history'

import * as authActionTypes from "./action-types";

import {
  authUser,
  fetchUserData,
  register,
  logoutUser,
} from "./effects";

function* loginUser(action) {
  try {
    const data = yield call(authUser, {
      email: action.payload.email,
      password: action.payload.password,
    });
    console.log('data', data)
    if (data.status === 200) {
      const userData = yield call(fetchUserData, { token: data.data });
      console.log(userData)
      const session = { token: data.data, data: userData.data };

      localStorage.setItem(process.env.REACT_APP_SESSION, JSON.stringify(session))
      yield put({
        type:
        authActionTypes.LOGIN_SUCCESS,
        data: {
          session,
        },
      });
      history.replace('/')
    }
    if (data.status === 401) {
      yield put({ type: authActionTypes.LOGIN_FAILED, data: { error: data.message } });
    }
    if (typeof data.errors !== 'undefined' && typeof data.errors.email !== 'undefined' && data.errors.email[0] === "Email is not yet confirmed.") {
      // NavigationService.navigate('EnterPin', { email: action.payload.email, password: action.payload.password })
    }
    // eslint-disable-next-line no-nested-ternary
    yield put({ type: authActionTypes.LOGIN_FAILED, data: { error: data.errors ? typeof data.errors.email !== 'undefined' ? data.errors.email : typeof data.errors.password !== 'undefined' ? data.errors.password : data.errors : data.status === 409 ? data.message : 'Invalid email or password' } });
  } catch (error) {
    console.log('Oops:', error);
    yield put({ type: authActionTypes.LOGIN_FAILED, data: { error: "Something went wrong" } });
  }
}

function* registerUser(action) {
  yield delay(1500);
  try {
    const {
      firstName,
      lastName,
      country,
      birthday,
      gender,
      email,
      password,
      passwordConfirmation,
    } = action.payload;

    if (password !== passwordConfirmation) {
      yield put({ type: authActionTypes.SIGNUP_FAILED, data: { error: { passwordConfirmation: ["Password does not match."] } } })
    } else {
      const reg = yield call(register, {
        firstName,
        lastName,
        country,
        birthday,
        gender,
        email,
        password,
      });
      if (reg.status === 201) {
        yield put({ type: authActionTypes.SIGNUP_SUCCESS });
        alert(
          'Success!',
          'Please check your email for the confirmation code.',
          [
            {
              text: 'OK',
              // onPress: () => NavigationService.navigate('EnterPin', { email, password }),
            },
          ],
          { cancelable: false },
        );
      } else {
        yield put({ type: authActionTypes.SIGNUP_FAILED, data: { error: reg.errors } })
      }
    }
  } catch (error) {
    const errors = {};
    yield put({ type: authActionTypes.SIGNUP_FAILED, data: { error: errors.firstName = "Network error." } })
    setTimeout(() => {
      alert(
        'Oops',
        'Network error.',
      );
    }, 100)
  }
}

function* signoutUser(action) {
  console.log('signout called')
  try {
    yield call(logoutUser, {
      token: action.payload.token,
    });
    yield localStorage.removeItem(process.env.REACT_APP_SESSION);
    yield delay(500)
    // NavigationService.navigate('Splash');
    yield put({ type: authActionTypes.LOGOUT_SUCCESS });
    yield delay(1000)
  } catch (error) {
    yield localStorage.removeItem(process.env.REACT_APP_SESSION);
    yield delay(500)
    // NavigationService.navigate('Splash');
    yield put({ type: authActionTypes.LOGOUT_SUCCESS });
    yield delay(1000)
  }
}

function* restoreSession() {
  try {
    const session = yield call(localStorage.getItem, process.env.REACT_APP_SESSION);
    const data = JSON.parse(session);
    if (session != null) {
      history.replace('/', {})
      yield put({ type: authActionTypes.LOGIN_SUCCESS, data: { session: data } });
    } else {
      yield put({ type: authActionTypes.RESTORE_FAILED });
    }
  } catch (error) {
    console.log('Oops:', error);
    yield put({ type: authActionTypes.RESTORE_FAILED });
    history.push('/login')
  }
}

const authSaga = [
  takeLatest(authActionTypes.LOGIN_REQUEST, loginUser),
  takeLatest(authActionTypes.SIGNUP_REQUEST, registerUser),
  takeLatest(authActionTypes.LOGOUT_REQUEST, signoutUser),
  takeLatest(authActionTypes.RESTORE_REQUEST, restoreSession),
];

export default authSaga;
