import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, applyMiddleware, combineReducers } from "redux";
import { Provider } from "react-redux";
import createSagaMiddleware from 'redux-saga';

import './index.css';
import App from './routes';
import * as serviceWorker from './serviceWorker';
import * as reducers from './redux/root-reducer'
import rootSaga from './redux/root-saga'
import { LOGOUT_SUCCESS } from './redux/Auth/action-types';

const sagaMiddleware = createSagaMiddleware();
const createStoreWithMiddleware = applyMiddleware(sagaMiddleware)(createStore);
const reducer = combineReducers(reducers);

const rootReducer = (state, action) => {
  if (action.type === LOGOUT_SUCCESS) { // If the user have successfully signed out and ended his/her session ignore the line below line 24
    // eslint-disable-next-line no-param-reassign
    state = undefined // Reset all state to remove cached data of the previous session
  }
  return reducer(state, action)
}

const store = createStoreWithMiddleware(rootReducer);

sagaMiddleware.run(rootSaga);

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root'),
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
