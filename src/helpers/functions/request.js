/* eslint-disable import/prefer-default-export */
import axios from 'axios'

const request = axios.create({
  baseURL: process.env.REACT_APP_DEV === 'yes' ? process.env.REACT_APP_DEV_API_URL : process.env.REACT_APP_DEV_API_URL,
  timeout: 5000,
  retry: 0,
})

request.interceptors.response.use(null, error => {
  if (error.config) {
    if (error.response) {
      if (error.response.status >= 200 && error.response.status < 505) {
        return Promise.resolve(error.response);
      }
    }
    if (error.config.retry < 2) {
      const config = {
        ...error.config,
        retry: error.config.retry + 1,
      }
      return request.request(config);
    }
  }
  return Promise.reject(error)
})

export {
  request,
};
