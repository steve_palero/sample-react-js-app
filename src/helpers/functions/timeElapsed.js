/* eslint-disable import/prefer-default-export */
const timeElapsed = date => {
  const since = new Date(new Date(date).toUTCString()).getTime();

  const elapsed = (new Date().getTime() - since) / 1000;

  if (elapsed >= 0) {
    const diff = {};

    diff.weeks = Math.floor((elapsed / 604800) % 7);
    diff.days = Math.floor(elapsed / 86400);
    diff.hours = Math.floor((elapsed / 3600) % 24);
    diff.minutes = Math.floor((elapsed / 60) % 60);
    diff.seconds = Math.floor(elapsed % 60);

    let elapsedTime = '';
    if (diff.weeks > 0) {
      if (diff.weeks > 1) {
        elapsedTime = `${diff.weeks} weeks ago.`;
      } else {
        elapsedTime = `${diff.weeks} week ago.`;
      }
    }

    if (diff.days > 0 && diff.weeks <= 0) {
      if (diff.days > 1) {
        elapsedTime = `${diff.days} days ago.`;
      } else {
        elapsedTime = `${diff.days} day ago.`;
      }
    }

    if (diff.hours > 0 && diff.days <= 0 && diff.weeks <= 0) {
      if (diff.hours > 1) {
        elapsedTime = `${diff.hours} hours ago.`;
      } else {
        elapsedTime = `${diff.hours} hour ago.`;
      }
    }

    if (diff.minutes > 0 && diff.hours <= 0 && diff.days <= 0 && diff.weeks <= 0) {
      if (diff.minutes > 1) {
        elapsedTime = `${diff.minutes} minutes ago.`;
      } else {
        elapsedTime = `${diff.minutes} minute ago`;
      }
    }

    if (diff.seconds > 0 && diff.minutes <= 0 && diff.hours <= 0 && diff.days <= 0 && diff.weeks <= 0) {
      if (diff.seconds > 1) {
        elapsedTime = `${diff.seconds} seconds ago.`;
      } else {
        elapsedTime = `${diff.seconds} socond ago.`;
      }
    }

    // console.log(`timeElapsed responded with: ${elapsedTime}`);
    return `${elapsedTime}`
  }

  // console.warn('Elapsed time lesser than 0, i.e. specified datetime is still in the future.');
  return `Elapsed time lesser than 0, i.e. specified datetime is still in the future.`
}

const dayElapsed = date => {
  const since = new Date(new Date(date).toUTCString());

  const oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
  const firstDate = new Date(since);
  const secondDate = new Date();

  const diffDays = Math.round((firstDate.getTime() - secondDate.getTime()) / (oneDay));

  return diffDays;
}

export {
  timeElapsed,
  dayElapsed,
};
