/* eslint-disable no-param-reassign */
/* eslint-disable import/prefer-default-export */
const dateFormatter = (date, format) => {
  function checkTime(i) {
    if (i < 10) {
      i = `0${i}`;
    }
    return i;
  }

  function clockFormat(i) {
    let timeFormat = "AM";
    if (i >= 12) {
      timeFormat = "PM";
    }

    return timeFormat;
  }

  function hourFormat(i) {
    if (i === 0) {
      i = 12;
    }
    if (i > 12) {
      i -= 12;
    }
    if (i < 10) {
      i = `0${i}`;
    }
    return i;
  }

  const initialDate = new Date(new Date(date).toUTCString());

  const weekday = [
    'Sunday',
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday',
  ];
  const month = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
  ];

  const abbWeekDay = [
    'Sun',
    'Mon',
    'Tues',
    'Wed',
    'Thurs',
    'Fri',
    'Sat',
  ];

  const abbMonth = [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'Jun',
    'Jul',
    'Aug',
    'Sept',
    'Oct',
    'Nov',
    'Dec',
  ];

  const h = initialDate.getHours();
  const m = initialDate.getMinutes();
  // const s = initialDate.getSeconds();

  const getWeekday = initialDate.getDay();
  const day = (`0${initialDate.getDate()}`).slice(-2);
  const getMonth = initialDate.getMonth();
  const year = initialDate.getFullYear();

  if (format === 'abb week day') {
    return `${abbWeekDay[getWeekday]}`
  }

  if (format === 'abb month') {
    return `${abbMonth[getMonth]}`
  }

  if (format === 'day') {
    return `${day}`
  }

  if (format === 'Weekday') {
    return weekday[getWeekday];
  }

  if (format === 'Weekday, Month Day Year') {
    return `${weekday[getWeekday]}, ${month[getMonth]} ${day}, ${year}`
  }

  if (format === 'Day Month, Year @HMS') {
    return `${day} ${month[getMonth]}, ${year} @${h}:${checkTime(m)}`;
  }

  if (format === 'Day Month, Year @ClockFormat') {
    return `${day} ${month[getMonth]}, ${year} @${hourFormat(h)}:${checkTime(m)} ${clockFormat(h)}`;
  }

  if (format === 'time') {
    return `${hourFormat(h)}:${checkTime(m)} ${clockFormat(h)}`;
  }

  if (format === 'time meridian') {
    return clockFormat(h);
  }

  if (format === 'Weekday Day, Month Year') {
    return `${weekday[getWeekday]} ${day}, ${month[getMonth]} ${year}`;
  }

  if (format === 'Weekday Month Day, Year') {
    return `${weekday[getWeekday]} ${month[getMonth]} ${day}, ${year}`;
  }

  if (format === 'MM/DD/YYYY') {
    const mon = getMonth + 1;
    return `${mon < 10 ? `0${mon}` : mon}/${day}/${year}`;
  }

  if (format === 'DD/MM/YYYY') {
    return `${day}/${getMonth + 1}/${year}`;
  }

  if (format === 'YYYY-MM-DD') {
    return `${year}-${(`0${getMonth + 1}`).slice(-2)}-${day}`;
  }


  if (format === 'YYYY/MM/DD') {
    return `${year}/${getMonth + 1}/${day}`;
  }

  if (format === 'Month Day, Year') {
    return `${month[getMonth]} ${day}, ${year}`;
  }

  if (format === 'Day Month, Year') {
    return `${day} ${month[getMonth]}, ${year}`;
  }

  if (format === 'abb Day Month Year') {
    return `${day} ${abbMonth[getMonth]} ${year}`;
  }

  if (format === 'Year, Month Day') {
    return `${year}, ${month[getMonth]} ${day}`;
  }

  console.error(`Date Error! with format ${format}!`);
  return "Date Error!"
}

export { dateFormatter };
