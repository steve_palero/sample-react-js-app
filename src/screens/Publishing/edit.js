/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { Component } from 'react'

import Lottie from 'react-lottie'
import { withRouter } from 'react-router-dom';

import react from '../../assets/images/react-logo.json'
import './publish.css'
import { request } from '../../helpers';

const bgImage = require('../../assets/images/1-nologo.gif')

const styles = {
  container: {
    backgroundImage: `url(${bgImage})`,
    backgroundRepeat: 'no-repeat',
    backgroundAttachment: 'fixed',
    backgroundPosition: 'center',
    backgroundSize: '100%',
    padding: 20,
    minHeight: '100vh',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
  },
  title: {
    alignSelf: 'center',
    fontSize: 40,
    fontFamily: 'permanentMarker',
    color: '#fff',
  },
  inputContainer: {
    alignSelf: 'center',
    width: '80%',
    margin: 80,
    borderRadius: 20,
    backgroundColor: '#fff',
    padding: 40,
  },
}

class Edit extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: '',
      description: '',


      errors: [],
      submitting: false,
    }
  }

  componentDidMount() {
    this.fetchBlog()
  }

  fetchBlog = async () => {
    this.setState({ submitting: true })
    try {
      const { match } = this.props;
      const { slug } = match.params;
      console.log('slug', slug)
      const data = await request.get(`/blogs/${slug}`)
      const response = data.data
      console.log('data', response)
      if (response.status === 200) {
        this.setState({
          title: response.data.blog.title,
          description: response.data.blog.description,
          submitting: false,
        })
      } else {
        alert.alert(
          'Ooops',
          'Failed to fetch blog.',
        )
        this.setState({
          submitting: false,
        })
      }
    } catch (e) {
      console.log(e)
    }
  }

  handleTitle = e => {
    this.setState({ title: e.target.value })
  }

  handleDescription = e => {
    this.setState({ description: e.target.value })
  }

  onSubmit = async () => {
    this.setState({ submitting: true })
    const {
      title,
      description,
    } = this.state;

    try {
      const { match } = this.props;
      const { slug } = match.params;
      const formData = new FormData()
      formData.append('title', title)
      formData.append('description', description)
      const data = await request.put(`/blogs/${slug}`, formData)
      const response = data.data;
      console.log('response', response)
      await setTimeout(() => this.setState({ submitting: false }), 2000)
      if (response.status !== 400) {
        const { history } = this.props;
        history.push('/')
      }
      this.setState({
        errors: response.errors,
      })
    } catch (e) {
      console.log(e)
    }
  }

  render() {
    const {
      title,
      description,
      submitting,
      errors,
    } = this.state;
    console.log('title', title)
    return (
      <div style={styles.container}>
        <p style={styles.title}>Publishing</p>
        {!submitting ? (
          <div style={styles.inputContainer}>
            <p />
            <label
              htmlFor={1}
              className="error"
            >
              {errors.filter(x => x.field === 'title').length > 0 ? errors.filter(x => x.field === 'title')[0].message : 'Title:'}
            </label>
            <div className="field active">
              <input
                id={1}
                type="text"
                value={title}
                placeholder="Title"
                onChange={this.handleTitle}
              />
            </div>

            <p />
            <label
              htmlFor={1}
              className="error"
            >
              {errors.filter(x => x.field === 'description').length > 0 ? errors.filter(x => x.field === 'description')[0].message : 'Description:'}
            </label>
            <div className="field active">
              <input
                id={2}
                type="text"
                value={description}
                placeholder="Description"
                onChange={this.handleDescription}
              />
            </div>

            <button
              type="button"
              className="button"
              onClick={this.onSubmit}
            >
              Submit
            </button>
          </div>
        ) : (
          <div style={styles.inputContainer}>
            <Lottie
              options={{
                loop: true,
                autoplay: true,
                animationData: react,
                rendererSettings: {
                  preserveAspectRatio: 'xMidYMid slice',
                },
              }}
              height={200}
              width={200}
            />
          </div>
        )}
      </div>
    )
  }
}

export default withRouter(Edit);
