import React, { Component } from 'react'

import Lottie from 'react-lottie'

import { Link } from 'react-router-dom'

import Icon from '@mdi/react'
import { mdiDelete, mdiPencil } from '@mdi/js'

import { connect } from 'react-redux'

import { RESTORE_REQUEST } from '../../redux/Auth/action-types'
import { request } from '../../helpers'
import react from '../../assets/images/react-logo.json'
import circleLoading from '../../assets/images/circle-loader.json'

import './home.css'

const bgImage = require('../../assets/images/1-nologo.gif')

const styles = {
  container: {
    backgroundImage: `url(${bgImage})`,
    backgroundRepeat: 'no-repeat',
    backgroundAttachment: 'fixed',
    backgroundPosition: 'center',
    backgroundSize: '100%',
    padding: 20,
    minHeight: '100vh',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
  },
  text: {
    fontFamily: 'permanentMarker',
    fontSize: 70,
    paddingLeft: 20,
    paddingRight: 20,
  },
  welcomeText: {
    alignSelf: 'center',
    fontSize: 40,
    fontFamily: 'permanentMarker',
    color: '#fff',
  },
  counterText: {
    alignSelf: 'center',
    fontSize: 30,
    color: '#000',
    fontFamily: 'permanentMarker',
  },
  blogContainer: {
    width: '50%',
    padding: 10,
    backgroundColor: '#fff',
    borderRadius: 10,
  },
  blogDescription: {
    alignSelf: 'center',
    fontSize: 20,
    fontFamily: 'gochiHand',
    color: '#000',
  },
}

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fetchingBlogs: false,
      blogs: [],

      loading: false,
    }
  }

  componentDidMount = async () => {
    const { dispatch, auth } = this.props;
    if (!auth.isAuth) {
      await dispatch({ type: RESTORE_REQUEST })
    }
    this.fetchBlogs()
    console.log('home mounted')
  }

  componentDidUpdate(prevProps) {
    const { auth } = this.props;
    if (prevProps.auth.isAuth !== auth.isAuth && auth.isAuth) {
      this.fetchBlogs()
    }
  }

  componentWillUnmount() {
    console.log('home unmounted')
  }

  fetchBlogs = async () => {
    this.setState({ loading: true })
    try {
      const { auth } = this.props;
      const data = await request.get('/blogs', {
        headers: {
          Authorization: `bearer ${auth.authSession.token}`,
        },
      })
      console.log('response', data)
      const response = data.data;
      setTimeout(() => {
        this.setState({ loading: false })
        if (response.status === 200) {
          this.setState({
            fetchingBlogs: false,
            blogs: response.data.blogs,
          })
        }
      }, 3000)
    } catch (e) {
      this.setState({ loading: false })
      console.log('error', e)
    }
  }

  deleteItem = async item => {
    try {
      const { auth } = this.props;
      const data = await request.delete(`/blogs/${item}`, {
        headers: {
          Authorization: `Bearer ${auth.authSession.token}`,
        },
      })
      const response = data.data;
      console.log('delete response', response)
      if (response.status === 200) {
        this.fetchBlogs();
      } else {
        alert(
          'Failed',
          'Failed to delete item.',
        )
      }
    } catch (e) {
      console.log(e)
    }
  }

  renderBlogs = () => {
    const { blogs } = this.state;
    if (blogs.length > 0) {
      return blogs.map((data, index) => (
        <div
          key={data._id}
          style={{
            width: '50%',
            padding: 10,
            margin: 30,
            backgroundColor: '#fff',
            borderRadius: 10,
            alignSelf: index % 2 === 1 ? 'flex-start' : 'flex-end',
          }}
        >
          <div
            style={{
              display: 'flex',
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
            }}
          >
            <Link
              to="/"
              style={{
                textDecoration: 'none',
              }}
            >
              <p style={styles.counterText}>{data.title}</p>
            </Link>

            <div
              style={{
                display: 'flex',
                flexDirection: 'row',
              }}
            >
              <Link
                type="button"
                className="homeButton"
                // onClick={() => this.deleteItem(data.slug)}
                to={`/blogs/edit/${data.slug}`}
              >
                <Icon
                  path={mdiPencil}
                  size="40px"
                  color="gray"
                />
              </Link>

              <button
                type="button"
                className="homeButton"
                onClick={() => this.deleteItem(data.slug)}
              >
                <Icon
                  path={mdiDelete}
                  size="40px"
                  color="gray"
                />
              </button>
            </div>
          </div>
          <br />
          <p style={styles.blogDescription}>{data.description}</p>
        </div>
      ))
    }
    return null
  }

  render() {
    const { auth } = this.props;
    console.log(auth)
    const {
      fetchingBlogs,
      blogs,
      loading,
    } = this.state;
    console.log('isFetching', fetchingBlogs)
    console.log('blogs', blogs)
    return (
      <div style={styles.container}>
        <p style={styles.welcomeText}>Things to Watch or Listen to</p>

        {loading && (
          <Lottie
            options={{
              loop: true,
              autoplay: true,
              animationData: circleLoading,
              rendererSettings: {
                preserveAspectRatio: 'xMidYMid slice',
              },
            }}
            height={300}
            width={300}
          />
        )}

        {fetchingBlogs ? (
          <Lottie
            options={{
              loop: true,
              autoplay: true,
              animationData: react,
              rendererSettings: {
                preserveAspectRatio: 'xMidYMid slice',
              },
            }}
            height={600}
            width={600}
          />
        )
          : this.renderBlogs()}
      </div>
    )
  }
}

const mapStateToProps = data => ({ auth: data.authenticate })

export default connect(mapStateToProps)(Home);
