/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { Component } from 'react'

import Lottie from 'react-lottie'

import { connect } from 'react-redux'

import { withRouter } from 'react-router-dom'

import react from '../../assets/images/react-logo.json'
import './publish.css'

import { LOGIN_REQUEST } from '../../redux/Auth/action-types';

const bgImage = require('../../assets/images/1-nologo.gif')

const styles = {
  container: {
    backgroundImage: `url(${bgImage})`,
    backgroundRepeat: 'no-repeat',
    backgroundAttachment: 'fixed',
    backgroundPosition: 'center',
    backgroundSize: '100%',
    padding: 20,
    minHeight: '100vh',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
  },
  title: {
    alignSelf: 'center',
    fontSize: 40,
    fontFamily: 'permanentMarker',
    color: '#fff',
  },
  inputContainer: {
    alignSelf: 'center',
    width: '80%',
    margin: 80,
    borderRadius: 20,
    backgroundColor: '#fff',
    padding: 40,
  },
}

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',


      errors: [],
      submitting: false,
    }
  }

  handleEmail = e => {
    this.setState({ email: e.target.value })
  }

  handlePassword = e => {
    this.setState({ password: e.target.value })
  }

  handleLoginRequest = () => {
    const { dispatch } = this.props;
    const { email, password } = this.state;
    dispatch({
      type: LOGIN_REQUEST,
      payload: {
        email,
        password,
      },
    });
  }

  render() {
    const {
      email,
      password,
      submitting,
      errors,
    } = this.state;
    const { auth } = this.props;
    console.log('auth', auth)
    console.log('email', email)
    return (
      <div style={styles.container}>
        <p style={styles.title}>Login</p>
        {!submitting ? (
          <div style={styles.inputContainer}>
            <p />
            <label
              htmlFor={1}
              className="error"
            >
              {errors.filter(x => x.field === 'email').length > 0 ? errors.filter(x => x.field === 'email')[0].message : 'Email:'}
            </label>
            <div className="field active">
              <input
                id={1}
                type="text"
                value={email}
                placeholder="Email"
                onChange={this.handleEmail}
              />
            </div>

            <p />
            <label
              htmlFor={1}
              className="error"
            >
              {errors.filter(x => x.field === 'password').length > 0 ? errors.filter(x => x.field === 'password')[0].message : 'Password:'}
            </label>
            <div className="field active">
              <input
                id={2}
                type="text"
                value={password}
                placeholder="Password"
                onChange={this.handlePassword}
              />
            </div>

            <button
              type="button"
              className="button"
              onClick={this.handleLoginRequest}
            >
              Submit
            </button>
          </div>
        ) : (
          <div style={styles.inputContainer}>
            <Lottie
              options={{
                loop: true,
                autoplay: true,
                animationData: react,
                rendererSettings: {
                  preserveAspectRatio: 'xMidYMid slice',
                },
              }}
              height={200}
              width={200}
            />
          </div>
        )}
      </div>
    )
  }
}

const mapStateToProps = data => ({ auth: data.authenticate })

export default withRouter(connect(mapStateToProps)(Login));
