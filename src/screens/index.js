/* eslint-disable import/prefer-default-export */
import Home from './Home'
import Publishing from './Publishing'
import Edit from './Publishing/edit'
import Login from './Auth'

export {
  Home,
  Publishing,
  Edit,
  Login,
}
